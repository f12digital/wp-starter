<footer class="global-footer">
    <div class="container">
    	<div class="grid">
    		<div class="grid__item col-1-1 m--col-1-2 l--col-1-4">
		    	<?php if( function_exists( 'client_footer_menu' ) ) { 
					echo '<nav class="global-footer__nav" id="footer_menu">'; 
						client_footer_menu(); 
					echo '</nav>';
				} ?>
			</div>
			<div class="grid__item col-1-1 m--col-1-2 l--col-1-4">
		    	<?php if( function_exists( 'client_footer_second_menu' ) ) { 
					echo '<nav class="global-footer-secondary__nav" id="footer_2_menu">'; 
						client_footer_second_menu(); 
					echo '</nav>';
				} ?>
			</div>
			<div class="grid__item col-1-1 m--col-1-2 l--col-1-4">
				<?php echo get_field('footer_column_3', 'options'); ?>
			</div>
			<div class="grid__item col-1-1 m--col-1-2 l--col-1-4">
				<?php echo get_field('footer_column_4', 'options'); ?>
			</div>
		</div>
		<div class="copyright_row"><span class="privacy"><a href="/privacy-policy/"><?php _e('Privacy Policy', 'def6'); ?></a></span><span class="copyright">&copy; <?php echo date('Y'); _e(' All Rights Reserved.', 'def6'); ?></span></div>
    </div>

</footer>

<?php if( is_user_logged_in() && current_user_can( 'edit_posts' ) ) { ?>
<nav class="logged-in-nav">
	<ul>
		<li>Logged In:</li>
		<li><a href="<?php echo admin_url(); ?>">Admin</a></li>
		<?php edit_post_link( __( 'Edit Post' ), '<li>', '</li>' );?>
	</ul>
</nav>
<?php }; ?>

<?php wp_footer(); ?>
<?php echo get_field('end_scripts', 'options'); ?>
</body>
</html>