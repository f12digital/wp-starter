jQuery(function($) {

    /* Get accurate Viewport sizing (no jQuery) */
    var viewportwidth;
    var viewportheight;

    function getWindowSize() {
        if (typeof window.innerWidth != 'undefined') {
            viewportwidth = window.innerWidth,
                viewportheight = window.innerHeight
        } else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) {
            viewportwidth = document.documentElement.clientWidth,
                viewportheight = document.documentElement.clientHeight
        } else {
            viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
                viewportheight = document.getElementsByTagName('body')[0].clientHeight
        }
    }

    /*
     *
     * Equal Height jQuery Snippet. 
     *
     */
    var equalheight = function(container) {

        var currentTallest = 0,
            currentRowStart = 0,
            rowDivs = [],
            $el,
            topPosition = 0,
            currentDiv;

        $(container).each(function() {

            $el = $(this);
            $el.height('auto');
            topPosition = $el.position().top;

            if (currentRowStart < topPosition - 1 || currentRowStart > topPosition + 1) {
                for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
                rowDivs = [];
                currentRowStart = topPosition;
                currentTallest = $el.height();
                rowDivs.push($el);
            } else {
                rowDivs.push($el);
                currentTallest = Math.max(currentTallest, $el.height());
            }
        });

        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
            rowDivs[currentDiv].css('min-height', currentTallest);
        }
    };


    /* Modal */
    function d6Modal() {
        var modalTrigger = $('a[name=modal]');

        //select all the a tag with name equal to modal
        modalTrigger.click(function(e) {
            e.preventDefault();
            //Get the A tag
            var id = $(this).attr('href');

            //Get the screen height and width
            var maskHeight = $(document).height();
            var maskWidth = $(window).width();

            //Set height and width to mask to fill up the whole screen
            $('#mask').css({
                'width': maskWidth,
                'height': maskHeight
            });

            //transition effect		
            $('#mask').fadeIn(1000);
            $('#mask').fadeTo("fast", 0.7);

            //Get the window height and width
            var winH = $(window).height();
            var winW = $(window).width();

            //Set the popup window to center
            $(id).css('top', winH / 2 - $(id).height() / 2);
            $(id).css('left', winW / 2 - $(id).width() / 2);

            //transition effect
            $(id).fadeIn(800, function() {
                if ($('.slick-slider').length) {
                    $('.modal_slides').resize();
                }
            });

            if ($(id).attr('aria-hidden', 'true')) {
                $(id).attr("aria-hidden", "false");
            } else {
                $(id).attr("aria-hidden", "true");
            }

            if (winW <= 600 && $(id).hasClass('modal--content')) {
                $('html,body').animate({
                        scrollTop: $(id).offset().top - 50
                    },
                    'slow');
            }
        });

        //if close button is clicked
        $('.modal .modal__close').click(function(e) {
            e.preventDefault();
            $('#mask').hide();
            $(this).parent('.modal').hide();

            if ($(this).parent('.modal').attr('aria-hidden', 'false')) {
                $(this).parent('.modal').attr("aria-hidden", "true");
            } else {
                $(this).parent('.modal').attr("aria-hidden", "false");
            }
        });

        //if mask is clicked
        $('#mask').click(function() {
            $(this).hide();
            $('.modal').hide();

            if ($('.modal__video').get(0).play()) {
                $('.modal__video').get(0).pause();
            }
        });
    }

    /* Global Vars */
    var $window = $(window),
        $document = $(document),
        scrolled = false,


        /******
        Start Run Functions
        ******/

        $document.ready(function(event) {

            getWindowSize();
            equalheight('.vertical_align');
            $window.width($window.width());

            //Open links in new window
            $("a[href^='http:']:not([href*='" + window.location.host + "']), a[href^='https:']:not([href*='" + window.location.host + "'])").each(function() {
                $(this).attr("target", "_blank");
            });

            if (window.location.hash) {
                if (this.hash !== "") {
                    // Store hash
                    var hash = window.location.hash;
                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top - 65
                    }, 800, function() {});

                } // End if
            }


        }); //end ready

    $window.on('load', function(event) {

        getWindowSize();
        equalheight('.vertical_align');
        $window.width($window.width());

    }); //end load

    $window.on('resize', function(event) {

        getWindowSize();
        equalheight('.vertical_align');

    }); //end resize

    window.onscroll = function() {

    }; //end scroll

}); //end main $ function