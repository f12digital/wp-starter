<?php
/**
 * WP Starter Theme functions and definitions
 *
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 */

/***************************************************
 * Definitions
 ***************************************************/
define( 'CORE_DIR', 'core/' );           // core directory
define( 'FUNCTIONS_DIR', 'functions/' ); // functions directory

define( 'CLIENT_URI', get_stylesheet_directory_uri() );
define( 'CLIENT_PATH', get_stylesheet_directory() );
define( 'CLIENT_ASSETS', 'assets' );
define( 'CLIENT_JS', 'js/min' );
define( 'CLIENT_CSS', 'css' );

define( 'FULL_CLIENT_JS_PATH', CLIENT_URI . '/' . CLIENT_ASSETS . '/' . CLIENT_JS . '/');
define( 'FULL_CLIENT_CSS_PATH', CLIENT_URI . '/' . CLIENT_ASSETS . '/' . CLIENT_CSS . '/');

/***************************************************
 * Core Files
 ***************************************************/
include_once( CORE_DIR . 'core.php' ); // Core

/***************************************************
 * Functions
 ***************************************************/
include_once( FUNCTIONS_DIR . 'acf_fields.php' );              // acf fields
include_once( FUNCTIONS_DIR . 'get_client_paging.php' );       // pagination
include_once( FUNCTIONS_DIR . 'limit_content.php' );           // excerpt alternative
include_once( FUNCTIONS_DIR . 'options.php' );                 // custom site options
include_once( FUNCTIONS_DIR . 'post-types.php' );              // custom post types
include_once( FUNCTIONS_DIR . 'theme_menus.php' );             // site menus

/**
 * Enqueue scripts and styles
 */

function def6_theme_scripts() {
/* CSS */
  wp_enqueue_style( 'style-main', FULL_CLIENT_CSS_PATH . 'style.css' );
/* JS */
  wp_enqueue_script( 'script-jquery', FULL_CLIENT_JS_PATH . 'jquery.min.js', array(), '2.2.2', true );
  wp_enqueue_script( 'slick-main', FULL_CLIENT_JS_PATH . 'slick.min.js', array(), '1.8.0', true );
  wp_enqueue_script( 'magnific-popup', FULL_CLIENT_JS_PATH . 'magnific-popup.min.js', array(), '1.1.0', true );
  wp_enqueue_script( 'masonry-main', FULL_CLIENT_JS_PATH . 'masonry.min.js', array(), '1.0.0', true );
  wp_enqueue_script( 'isonscreen-main', FULL_CLIENT_JS_PATH . 'jquery.isonscreen.min.js', array(), '1.0.0', true );
  wp_enqueue_script( 'script-main', FULL_CLIENT_JS_PATH . 'main.min.js', array(), '1.0.0', true );
  wp_enqueue_script( 'player-main', 'https://player.vimeo.com/api/player.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'def6_theme_scripts' );


/**************************************************
 * Set up theme defaults and registers support
 * for various WordPress features.
 ***************************************************/
/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );

/**
 * Add excerpt support to pages
 */
add_post_type_support( 'page', 'excerpt' );

/**
 * Add featured images
 */
add_theme_support( 'post-thumbnails' );

/*
 * Switch default core markup for search form, comment form, and comments
 * to output valid HTML5.
 */
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );


/***************************************************
 * Hooks
 ***************************************************/
/**
 * Paging
 */
add_action( '_after_index_loop', 'get_client_paging', 10 ); // index.php
add_action( '_after_search_loop', 'get_client_paging', 10 ); // search.php


/* Hide WP version strings from scripts and styles
 * @return {string} $src
 * @filter script_loader_src
 * @filter style_loader_src
 */
function def6_remove_wp_version_strings( $src ) {
 global $wp_version;
 parse_str(parse_url($src, PHP_URL_QUERY), $query);
 if ( !empty($query['ver']) && $query['ver'] === $wp_version ) {
 $src = remove_query_arg('ver', $src);
 }
 return $src;
}
add_filter( 'script_loader_src', 'def6_remove_wp_version_strings' );
add_filter( 'style_loader_src', 'def6_remove_wp_version_strings' );

/* Hide WP version strings from generator meta tag */
function def6_remove_version() {
return '';
}
add_filter('the_generator', 'def6_remove_version');

/***
*
* Add Post counts to admin for custom post types
*
***/
add_action('manage_users_columns','def6_manage_users_columns');
function def6_manage_users_columns($column_headers) {
    unset($column_headers['posts']);
    $column_headers['custom_posts'] = 'Posts';
    return $column_headers;
}

add_action('manage_users_custom_column','def6_manage_users_custom_column',10,3);
function def6_manage_users_custom_column($custom_column,$column_name,$user_id) {
    if ($column_name=='custom_posts') {
        $counts = _def6_get_author_post_type_counts();
        $custom_column = array();
        if (isset($counts[$user_id]) && is_array($counts[$user_id]))
            foreach($counts[$user_id] as $count) {
                $link = admin_url() . "edit.php?post_type=" . $count['type']. "&author=".$user_id;
                // admin_url() . "edit.php?author=" . $user->ID;
                $custom_column[] = "\t<tr><th><a href={$link}>{$count['label']}</a></th><td>{$count['count']}</td></tr>";
            }
        $custom_column = implode("\n",$custom_column);
        if (empty($custom_column))
            $custom_column = "<th>[none]</th>";
        $custom_column = "<table>\n{$custom_column}\n</table>";
    }
    return $custom_column;
}

function _def6_get_author_post_type_counts() {
    static $counts;
    if (!isset($counts)) {
        global $wpdb;
        global $wp_post_types;
        $sql = <<<SQL
        SELECT
        post_type,
        post_author,
        COUNT(*) AS post_count
        FROM
        {$wpdb->posts}
        WHERE 1=1
        AND post_type NOT IN ('revision','nav_menu_item','acf-field-group','acf-field')
        AND post_status IN ('publish','pending', 'draft')
        GROUP BY
        post_type,
        post_author
SQL;
        $posts = $wpdb->get_results($sql);
        foreach($posts as $post) {
            $post_type_object = $wp_post_types[$post_type = $post->post_type];
            if (!empty($post_type_object->label))
                $label = $post_type_object->label;
            else if (!empty($post_type_object->labels->name))
                $label = $post_type_object->labels->name;
            else
                $label = ucfirst(str_replace(array('-','_'),' ',$post_type));
            if (!isset($counts[$post_author = $post->post_author]))
                $counts[$post_author] = array();
            $counts[$post_author][] = array(
                'label' => $label,
                'count' => $post->post_count,
                'type' => $post->post_type,
                );
        }
    }
    return $counts;
}