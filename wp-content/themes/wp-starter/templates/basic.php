<?php 
/*
**	This template is used when there are no modules on a page. 
*/ 
?>
<section class="basic-page container-small">

<?php the_title(); ?>

<?php if ( has_post_thumbnail() ) {
	the_post_thumbnail();
}  ?>

<?php the_content(); ?>

</section>