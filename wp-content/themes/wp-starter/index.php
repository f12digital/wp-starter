<?php get_header(); ?>

<?php if (get_the_content() && have_rows('page_builder') ) {
    echo '<section class="main_content container-small">';
        the_content();
    echo '</section>';
} ?>

<?php
// check if the flexible content field has rows of data
if( have_rows('page_builder') ):
     // loop through the rows of data
    while ( have_rows('page_builder') ) : the_row();
        
        //section wide
        $background = get_sub_field('background');
        $bg_image = get_sub_field('bg_image');
        $text_alignment = get_sub_field('text_alignment');
        $margin = get_sub_field('margin');
        $padding = get_sub_field('padding');

        $wrap_classes = '';
        
        if($background){
            $wrap_classes .= ' '.$background;
        }
        if($text_alignment) {
            $wrap_classes .= ' '.$text_alignment;
        }
        if($margin) {
            $wrap_classes .= ' '.implode(' ', $margin);
        }
        if($padding) {
            $wrap_classes .= ' '.implode(' ', $padding);
        }
        if($background == 'bg-image'){ 
            $wrap_classes .= '" style="background-image:url('. $bg_image .')'; 
        } 

        echo '<section class="'.get_row_layout() . $wrap_classes.'" id="'.get_row_layout() .'">';
            get_template_part('template-parts/section-content');
        	get_template_part('template-parts/module', get_row_layout());
            get_template_part('template-parts/section-button');
        echo '</section>';
        wp_reset_postdata();
    endwhile;
else :
    // no modules found
    get_template_part('templates/basic');
endif;
?>

<?php get_footer(); ?>