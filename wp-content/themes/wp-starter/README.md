# README #

* Plugins *
- Advanced Custom Fields PRO

* Site Functions *
- Flexible content modules using ACF for page builder.

* Team Info *
- Account Executive: 
- Project Manager: 
- UX Design:
- Creative: 
- Development:
- Quality Assurance: 
- Content: 

* Technical *
- ACF fields created using acf php (https://www.advancedcustomfields.com/resources/register-fields-via-php/).

** Required **
- ACF PRO plugin.
- jquery.
