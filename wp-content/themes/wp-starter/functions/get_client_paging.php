<?php
/**
 * Client Paging
 *
 * @return string pagination
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function get_client_paging( $echo = false ) {
	global $wp_query;

	$bignum = 999999999;

	if ( $wp_query->max_num_pages <= 1 )
		return;

	$output = '<nav class="pagination">';

		$output .= paginate_links(
			array(
				'base' 		=> str_replace( $bignum, '%#%', esc_url( get_pagenum_link($bignum) ) ),
				'format' 	=> '',
				'current' 	=> max( 1, get_query_var('paged') ),
				'total' 	=> $wp_query->max_num_pages,
				'prev_text' => __( '<span>&laquo;</span> Previous', 'def6' ),
				'next_text' => __( 'Next <span>&raquo;</span>', 'def6' ),
				'type'		=> 'list',
				'end_size'	=> 3,
				'mid_size'	=> 3
			)
		);

	$output .= '</nav>';

	if( $echo == true ) {
		echo $output;
	} else {
		return $output;
	}
}