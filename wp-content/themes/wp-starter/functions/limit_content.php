<?php
	/**
	 * Limit Content
	 * @param  string  $content                content being limited
	 * @param  integer $max_char               trim length
	 * @param  string  $more_link_text         read more link text if used
	 * @param  string  $more_link_trailer      after trimmed text
	 * @param  boolean $display_read_more_link show the read more link
	 * @param  string  $custom_url             link to a specific url or permalink
	 * @param  string  $custom_class           custom class on the read more link
	 * @param  string  $wrapper                custom wrapper for the trimmed text
	 * @return string                          trimmed content
	 */
	function limit_content( $content = '', $max_char = 100, $more_link_text = '', $more_link_trailer = '...', $display_read_more_link = false, $custom_url = '', $custom_class = 'read-more', $wrapper = '' ) {
		$content = apply_filters( 'the_content', $content );
		$content = str_replace( ']]>', ']]&gt;', $content );
		$content = strip_tags( $content );

		if ( ( strlen( $content ) > $max_char ) && ( $spaces = strpos( $content, " ", $max_char ) ) ) {
			$content = substr( $content, 0, $spaces );
			$content = $content;
			$content .= $more_link_trailer;
		} else {
			$content = $content;
		};

		$classes = ( $custom_class ) ? ' class="' . $custom_class . '"' : '';

		if( $display_read_more_link == true ) {
			if( $custom_url != '' ){
				$content .= ' <a href="' . $custom_url . '"' . $classes . '>' . $more_link_text . '</a>';
			} else {
				$content .= ' <a href="' . get_permalink() . '"' . $classes . '>' . $more_link_text . '</a>';
			}
		}

		if( $wrapper ) {
			return '<' . $wrapper . '>' . $content . '</' . $wrapper . '>';
		} else {
			return $content;
		}
	}


/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );


/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 14;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );





