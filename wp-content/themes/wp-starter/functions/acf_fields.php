<?php 

if( function_exists('acf_add_local_field_group') ):

    // GLOBALS
    include('acf_fields/global_theme_settings.php');

    // POST TYPES
    /* put includes here */

    //FLEX CONTENT & MODULES
    include('acf_fields/page_builder.php');

endif;