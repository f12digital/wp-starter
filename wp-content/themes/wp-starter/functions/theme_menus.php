<?php
/**
 * Client Theme Menus
 *
 * All of the theme menus and interactions are registered here
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Menu Hooks
 */
add_filter( 'nav_menu_css_class', 'def6_nav_menu_css_class', 10, 2 );
add_filter( 'wp_nav_menu_args', 'def6_nav_menu_args' );

/**
 * Menu / Navigation
 */
register_nav_menus(
	array(
		'main-nav'		=> __( 'Main Menu', 'def6' ),
		'footer-nav'	=> __( 'Footer - Primary', 'def6' ),
		'footer-2-nav'	=> __( 'Footer - Secondary', 'def6' )
	)
);

// main menu
if( ! function_exists('client_main_menu') && has_nav_menu( 'main-nav' ) ) {
	function client_main_menu() {
		wp_nav_menu(
			array(
				'container'       => false,
				'items_wrap'	  => '',
				'menu'            => __( 'Main Menu', 'def6' ),
				'menu_class'      => 'main-nav',
				'theme_location'  => 'main-nav',
				'depth'           => 0,
				'link_before'     => '<span>',
				'link_after'      => '</span>',
			)
		);
	}
}

// footer menu
if( ! function_exists('client_footer_menu') && has_nav_menu( 'footer-nav' ) ) {
	function client_footer_menu() {
		wp_nav_menu(
			array(
				'container'       => false,
				'items_wrap'	  => '',
				'menu'            => __( 'Footer - Primary', 'def6' ),
				'menu_class'      => 'footer-nav',
				'theme_location'  => 'footer-nav',
				'depth'           => 0,
			)
		);
	}
}


// footer 2 menu
if( ! function_exists('client_footer_second_menu') && has_nav_menu( 'footer-2-nav' ) ) {
	function client_footer_second_menu() {
		wp_nav_menu(
			array(
				'container'       => false,
				'items_wrap'	  => '',
				'menu'            => __( 'Footer - Primary', 'def6' ),
				'menu_class'      => 'footer-nav',
				'theme_location'  => 'footer-2-nav',
				'depth'           => 0,
			)
		);
	}
}


/**
 * Cleaner walker for wp_nav_menu()
 *
 * Walker_Nav_Menu (WordPress default) example output:
 *   <li id="menu-item-8" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8"><a href="/">Home</a></li>
 *   <li id="menu-item-9" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9"><a href="/sample-page/">Sample Page</a></l
 *
 * def6_Custom_Walker example output:
 *   <li class="menu-home"><a href="/">Home</a></li>
 *   <li class="menu-sample-page"><a href="/sample-page/">Sample Page</a></li>
 */
class def6_Custom_Walker extends Walker_Nav_Menu {
	function check_current( $classes ) {
		return preg_match( '/(current[-_])|active|dropdown__item/', $classes );
	}

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		switch($depth) {
			case 0:
				$depth_word = 'zero';
				break;
			case 1:
				$depth_word = 'one';
				break;
			case 2:
				$depth_word = 'two';
				break;
			case 3:
				$depth_word = 'three';
				break;
			case 4:
				$depth_word = 'four';
				break;
			case 5:
				$depth_word = 'five';
				break;
			case 6:
				$depth_word = 'six';
				break;
			case 7:
				$depth_word = 'seven';
				break;
			case 8:
				$depth_word = 'eight';
				break;
			case 9:
				$depth_word = 'nine';
				break;
			case 10:
				$depth_word = 'ten';
				break;
			default:
				break;
		}
		$depth_class = " depth--" . $depth_word;
		$output .= "\n<ul class=\"dropdown" . $depth_class . "\">\n";
	}


	function display_element( $element, &$children_elements, $max_depth, $depth = 0, $args, &$output ) {
		$element->is_dropdown = ( ( ! empty($children_elements[$element->ID] ) && ( ( $depth + 1 ) < $max_depth || ( $max_depth === 0 ) ) ) );

		if ( $element->is_dropdown ) {
			$element->classes[] = 'dropdown__item';
		}

		parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}
}

/**
 * Remove the id="" on nav menu items
 * Return 'menu-slug' for nav menu classes
 */
function def6_nav_menu_css_class( $classes, $item ) {
	$slug = sanitize_title( $item->title );
	$classes = preg_replace( '/(-menu-|[-_]page[-_])/', '', $classes );
	$classes = preg_replace( '/(current)/', 'current--', $classes );
	$classes = preg_replace( '/^((menu|page)[-_\w+]+)+/', '', $classes );

	$classes[] = 'menu-' . $slug;

	$classes = array_unique( $classes );

	return array_filter( $classes );
}

/**
 * Clean up wp_nav_menu_args
 *
 * Remove the container
 * Use def6_Custom_Walker() by default
 */
function def6_nav_menu_args( $args = '' ) {
	$def6_nav_menu_args['container'] = false;

	if ( ! $args['items_wrap'] ) {
		$def6_nav_menu_args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
	}

	if ( ! $args['walker'] ) {
		$def6_nav_menu_args['walker'] = new def6_Custom_Walker();
	}

	return array_merge( $args, $def6_nav_menu_args );
}