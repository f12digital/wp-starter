var gulp = require('gulp');
var sass = require('gulp-sass');
// var browserSync = require('browser-sync');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var beautify = require('gulp-jsbeautify');
var gulpIf = require('gulp-if');
// var cssnano = require('gulp-cssnano');
// var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
// var del = require('del');
var runSequence = require('run-sequence');
var cleanCSS = require('gulp-clean-css');
var cssbeautify = require('gulp-cssbeautify');
var pump = require('pump');



// Basic Gulp task syntax
// gulp.task('hello', function() {
//   console.log('Hello Zell!');
// })

// Development Tasks 
// -----------------

gulp.task('sass', function() {
  return gulp.src('assets/scss/*.scss') // Gets all files ending with .scss in app/scss and children dirs
    .pipe(sass({outFile: 'assets/css/style.css'}).on('error', sass.logError)) // Passes it through a gulp-sass, log errors to console
    .pipe(gulp.dest('assets/css/')); // Outputs it in the css folder
})

gulp.task('watch', function() {
  gulp.watch('assets/scss/**/*.scss', ['sass']);
  gulp.watch('assets/*.html');
  gulp.watch('assets/js/**/*.js');
})

// beautify css
gulp.task('beautify-css', function() {
    return gulp.src('*.css')
        .pipe(cssbeautify())
        .pipe(gulp.dest(''));
});

// beautify js
gulp.task('beautify-js', function() {
  gulp.src('assets/js/*.js')
    .pipe(beautify({indentSize: 2}))
    .pipe(gulp.dest('assets/js/'));
});

// Optimization Tasks 
// ------------------
gulp.task('minify-css', () => {
  return gulp.src('*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest(''));
});

// minify js files
gulp.task('compress', function (cb) {
  pump([
        gulp.src('assets/js/*.js'),
        uglify(),
        gulp.dest('assets/js/')
    ],
    cb
  );
});



gulp.task('default', function(callback) {
  runSequence(['sass', 'beautify-css', 'beautify-js'], 'watch',
    callback
  )
})

gulp.task('build', function(callback) {
  runSequence(
    'sass',
    'compress',
    'minify-css',
    callback
  )
})
