<!DOCTYPE html>
<html class="no-js desktop-layout" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#000">
	<meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>
	<?php echo get_field('head_scripts', 'options'); ?>
</head>

<body <?php body_class(); ?>>
	
<?php echo get_field('body_scripts', 'options'); ?>

<div id="mm_container"></div>

<header class="global-header" id="global-header">
	
	<div class="global-header__wrap">

			<?php 
			$logo_src = get_field('logo', 'options');
			if($logo_src) {
				$logo = $logo_src["url"];
			} else {
				$logo = get_bloginfo( 'stylesheet_directory' ) . '/assets/images/logo.png'; 
			} ?>

			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="global-header__logo"><img src="<?php echo $logo; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' , 'display') ); ?>"></a>
			
			<div class="mobile_menu"><a href="#mm_container" id="mm_btn" aria-hidden="true"><span class="icon icon-menu"></span></a></div>
			
			<?php if( function_exists( 'client_main_menu' ) ) { 
				echo '<nav class="global-header__nav" id="main_menu">'; 
					echo '<a href="" class="back_to_mainMenu"><i class="icon icon-arrow-left-thick"></i> Main Menu</a>';
					client_main_menu(); 
				echo '</nav>';
			} ?>

	</div>

	<div class="clearfix"></div>

</header>