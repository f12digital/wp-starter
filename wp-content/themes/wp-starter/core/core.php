<?php
/**
 * Core Functions
 *
 * Functions used for hooks and site modifiers
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Admin Only
 */
if ( is_admin() ) :
	// Theme Activation
	add_action( 'after_switch_theme', 'def6_activation', 10, 2 );

	// Theme Deactivation
	add_action( 'switch_theme', 'def6_deactivation', 10, 2 );

endif;

/**
 * Theme Activation
 */
function def6_activation() {
}

/**
 * Theme Deactivation
 */
function def6_deactivation() {
}

// Remove all theme and plugin editing from the admin
define( 'DISALLOW_FILE_MODS', true );

/**
 * Tree function
 * https://github.com/chriscoyier/css-tricks-functionality-plugin/blob/master/includes/template-functions.php
 */
function is_tree( $pid ) {
	global $post;

	if ( is_page( $pid ) ) {
		return true;
	}

	$anc = get_post_ancestors( $post->ID );

	foreach ( $anc as $ancestor ) {
		if ( is_page() && $ancestor == $pid ) {
			return true;
		}
	}

	return false;
}



/**
 * Passthrough function for extended mobile detection querying
 * ie isIOS, isAndroidOS, isIpad
 */
global $detect;

function def6_mobile_detect($method){
	global $detect;

	if( ! is_object($detect) ){
		return false;
	}

	return $detect->$method();
}

//we want to set these constants globally to prevent multiple function calls
$detection_array = array(
	'SITE_IS_MOBILE' => 'isMobile',
	'SITE_IS_TABLET' => 'isTablet',
);

require_once 'inc/Mobile_Detect.php';
$detect = new Mobile_Detect;

//default all to false if we don't care about using mobile detection
foreach($detection_array as $const=>$method){
	define( $const, def6_mobile_detect($method) );
}

define( 'SITE_IS_DESKTOP', ( ! SITE_IS_MOBILE && ! SITE_IS_TABLET ) );