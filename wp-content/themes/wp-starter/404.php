<?php get_header(); ?>

<section class="404">
	<div class="404-wrap">
		<span class="h1">Oops, the page you visited doesn't exist!</span>
		<span class="h2">Didn't find what you were looking for?</span>
		<p class="cta_content">You may  have visited a page that does not exist, but while you are here, check out some other pages on our site.</p>
	</div>
</section>
	
<?php get_footer(); ?>