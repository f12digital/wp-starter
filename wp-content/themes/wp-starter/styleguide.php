<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>def6</title>
		<link rel="stylesheet" href="assets/css/style.css">
		<!--[if lt IE 9]>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
		<![endif]-->
		<script src="assets/js/min/jquery.min.js"></script>
		<script src="assets/js/min/jquery-migrate-1.2.1.min.js"></script>
		<script src="assets/js/min/jquery.isonscreen.min.js"></script>
		<script src="assets/js/min/main.min.js"></script>
		<script src="assets/js/min/modernizr.min.js"></script>
		<script src="assets/js/min/slick.min.js"></script>
	</head>
	<body>
		<div class="container">
			<?php /* Typography */ ?>
			<h1 class="styleguide_title">Typography:</h1>
			<hr>
			<h1>Heading 1</h1>
			<h2>Heading 2</h2>
			<h3>Heading 3</h3>
			<h4>Heading 4</h4>
			<h5>Heading 5</h5>
			<h6>Heading 6</h6>
			<span class="title">Title</span>
			<span class="title_sub">Sub Title</span>
			<span class="title_label">Title Label</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ligula nibh, fringilla id lacus sed, egestas consectetur nunc. Sed fermentum commodo vehicula. In elit nulla, porta nec leo in, ultrices scelerisque metus. Etiam pretium vel nisi non porttitor. Ut porta sollicitudin ex, sed ornare lorem maximus hendrerit. Cras ac massa tortor. Aenean vitae egestas nulla. Nullam facilisis arcu nulla. Curabitur id tortor volutpat, sagittis mauris sit amet, ultrices ligula. Maecenas fringilla luctus lectus nec vulputate. Nam pulvinar nisi vel congue ullamcorper. Suspendisse tincidunt mauris non imperdiet gravida. Phasellus molestie sollicitudin ex ac scelerisque.</p>

			<p>In hac habitasse platea dictumst. Sed dictum augue vel felis lobortis, varius scelerisque felis mattis. Nam semper, risus vitae fermentum sagittis, turpis nulla rhoncus nisl, sed cursus orci metus eu augue. Pellentesque vitae pretium nulla. Cras tempus efficitur neque, vel posuere magna luctus ac. Maecenas bibendum egestas viverra. Morbi arcu ex, semper sit amet rutrum a, feugiat non lorem. Pellentesque iaculis, enim id tempor vehicula, turpis felis dapibus lectus, vitae hendrerit augue nulla ut sem. Nulla metus dolor, iaculis et mauris eu, feugiat pellentesque neque. Suspendisse bibendum quis eros non placerat. Nullam volutpat volutpat nisi, ut consequat diam iaculis a. Curabitur a ligula lorem. Nulla fermentum felis orci, a fringilla odio volutpat ac. Nunc non faucibus tellus. Aenean et ex eu purus pharetra dapibus non quis massa. In et iaculis urna.</p>

			<p>Mauris fringilla magna et odio faucibus, ac placerat ex ornare. Vivamus lobortis nulla vitae purus dapibus, eget luctus justo porta. Phasellus sollicitudin pretium ipsum ut pulvinar. Proin ac mauris accumsan ante suscipit tincidunt. Curabitur non nulla nulla. Duis sagittis congue metus, non ornare elit ullamcorper nec. Ut interdum mauris felis, a volutpat massa pharetra sit amet. Proin maximus diam ex, sit amet gravida mi tempor vitae. Proin a sapien urna.</p>
			<ul>
				<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
				<li>Duis volutpat sem at lorem placerat, sit amet tincidunt lorem tincidunt.</li>
				<li>Ut luctus quam sed elementum imperdiet.
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
					</ul>
				</li>
				<li>Sed euismod nulla nec mattis pulvinar.</li>
				<li>Curabitur condimentum mauris nec risus aliquam, sit amet consectetur velit euismod.</li>
			</ul>
			<ul>
				<li>Vivamus quis est laoreet, placerat magna eget, vestibulum leo.</li>
				<li>Sed non odio ut quam placerat eleifend ac id mauris.</li>
				<li>Aliquam vitae turpis eu mi lobortis tempus.</li>
				<li>Suspendisse vel est volutpat, elementum lectus vitae, ornare purus.</li>
				<li>Integer ac arcu quis justo efficitur consequat vitae nec tortor.</li>
			</ul>
			<ul>
				<li>Duis at lectus auctor lacus gravida gravida.</li>
				<li>Morbi lacinia nulla non mi ornare, sed venenatis tellus elementum.</li>
			</ul>
			<p>Mauris fringilla magna et odio faucibus, ac placerat ex ornare. Vivamus lobortis nulla vitae purus dapibus, eget luctus justo porta. Phasellus sollicitudin pretium ipsum ut pulvinar. Proin ac mauris accumsan ante suscipit tincidunt. Curabitur non nulla nulla. Duis sagittis congue metus, non ornare elit ullamcorper nec. Ut interdum mauris felis, a volutpat massa pharetra sit amet. Proin maximus diam ex, sit amet gravida mi tempor vitae. Proin a sapien urna.</p>
			<ul>
				<li>Aliquam vel velit ut est mollis hendrerit id vel nisl.</li>
			</ul>
			<p>Mauris fringilla magna et odio faucibus, ac placerat ex ornare. Vivamus lobortis nulla vitae purus dapibus, eget luctus justo porta. Phasellus sollicitudin pretium ipsum ut pulvinar. Proin ac mauris accumsan ante suscipit tincidunt. Curabitur non nulla nulla. Duis sagittis congue metus, non ornare elit ullamcorper nec. Ut interdum mauris felis, a volutpat massa pharetra sit amet. Proin maximus diam ex, sit amet gravida mi tempor vitae. Proin a sapien urna.</p>

			<?php /* Grid */ ?>
			<hr>
			<h1>Grid:</h1>
			<hr>
			<div class="grid">
				<div class="grid__item col-1-1">col-1-1</div>
				<div class="grid__item col-1-1 s--col-1-2">col-1-1 s--col-1-2</div>
				<div class="grid__item col-1-1 s--col-1-2">col-1-1 s--col-1-2</div>
				<div class="grid__item col-1-1 m--col-1-3">col-1-1 m--col-1-3</div>
				<div class="grid__item col-1-1 m--col-1-3">col-1-1 m--col-1-3</div>
				<div class="grid__item col-1-1 m--col-1-3">col-1-1 m--col-1-3</div>
				<div class="grid__item col-1-1 s--col-1-2 m--col-1-4">col-1-1 s--col-1-2 m--col-1-4</div>
				<div class="grid__item col-1-1 s--col-1-2 m--col-1-4">col-1-1 s--col-1-2 m--col-1-4</div>
				<div class="grid__item col-1-1 s--col-1-2 m--col-1-4">col-1-1 s--col-1-2 m--col-1-4</div>
				<div class="grid__item col-1-1 s--col-1-2 m--col-1-4">col-1-1 s--col-1-2 m--col-1-4</div>
				<div class="grid__item col-1-1 s--col-1-3 m--col-1-5">col-1-1 s--col-1-3 m--col-1-5</div>
				<div class="grid__item col-1-1 s--col-1-3 m--col-1-5">col-1-1 s--col-1-3 m--col-1-5</div>
				<div class="grid__item col-1-1 s--col-1-3 m--col-1-5">col-1-1 s--col-1-3 m--col-1-5</div>
				<div class="grid__item col-1-1 s--col-1-3 m--col-1-5">col-1-1 s--col-1-3 m--col-1-5</div>
				<div class="grid__item col-1-1 s--col-1-3 m--col-1-5">col-1-1 s--col-1-3 m--col-1-5</div>
				<div class="grid__item col-1-1 s--col-1-3 m--col-1-6">col-1-1 s--col-1-3 m--col-1-6</div>
				<div class="grid__item col-1-1 s--col-1-3 m--col-1-6">col-1-1 s--col-1-3 m--col-1-6</div>
				<div class="grid__item col-1-1 s--col-1-3 m--col-1-6">col-1-1 s--col-1-3 m--col-1-6</div>
				<div class="grid__item col-1-1 s--col-1-3 m--col-1-6">col-1-1 s--col-1-3 m--col-1-6</div>
				<div class="grid__item col-1-1 s--col-1-3 m--col-1-6">col-1-1 s--col-1-3 m--col-1-6</div>
				<div class="grid__item col-1-1 s--col-1-3 m--col-1-6">col-1-1 s--col-1-3 m--col-1-6</div>
			</div>

			<?php /* Buttons */ ?>
			<hr>
			<h1>Buttons:</h1>
			<hr>
			<a href="#" class="button">button</a>
			<a href="#" class="button primary">button primary</a>
			<a href="#" class="button secondary">button secondary</a>
			<a href="#" class="more_link">more_link</a>


			<?php /* Colors */ ?>
			<hr>
			<h1>Colors:</h1>
			<hr>
			<span class="bg-primary">Primary Color</span>
			<span class="bg-secondary">secondary Color</span>
			<span class="bg-tertiary-color-one">tertiary-color-one Color</span>
		</div>
	</body>
</html>